import math

import numpy as np


class CityIntersections(object):
    def __init__(self, counting_engine):
        self.boxes = None
        self.total_intersections_number = None
        self.counting_engine = counting_engine
        self.box_size = self.counting_engine.box_size
        self.probabilities = None
        self.Z = {}         # sum of probabilities raised to the power of q

    def makeBoxes(self):
        self.boxes = np.fromiter(self.counting_engine, dtype=np.uint)

    def setProbabilities(self):
        self.probabilities = self.boxes / np.sum(self.boxes)

    def calculatePartitionFunction(self, q):
        return np.sum(self.probabilities ** q)

    def getA(self, q):
        # Equation (B3) in the article
        Z = self.Z.setdefault(q, self.calculatePartitionFunction(q))
        return np.sum(np.log(self.probabilities) * (self.probabilities ** q / Z))

    def getTau_version2(self, q):
        # Equation 4. https://imagej.nih.gov/ij/plugins/fraclac/FLHelp/MultifractalOptions.htm#tau
        return self.Z.setdefault(q-1, self.calculatePartitionFunction(q-1)) / len(self.boxes)

    def getTau(self, q):
        # Equations (9),(10) in the article
        return math.log(self.Z.setdefault(q, self.calculatePartitionFunction(q)))

    def getDWhenQIs1(self):
        # Equation (13) in the article
        return np.sum(np.log(self.probabilities) * self.probabilities)

    def getF(self, q):
        # Eq (B4) in the article
        Z = self.Z.setdefault(q, self.calculatePartitionFunction(q))
        logZ = math.log(Z)
        return np.sum((q*np.log(self.probabilities)-logZ) * self.probabilities **q / Z)

    def run(self):
        self.makeBoxes()
        self.setProbabilities()

