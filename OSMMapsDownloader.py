import osmnx as ox


class OSMMapsDownloader:
    """
    OSMMapsDownloader is used to download image with intersections.
    """

    def __init__(self, file_name, place, network_type='multifractal_service', fig_height=70, node_size=0.01, file_format='png'):
        """
        :param file_name: output file name
        :param place: Place, whose map is being downloaded
        :param network_type: multifracial is not included in osmnx package. It is a combination of 'walk' and 'drive' network types
        :param fig_height: parameter, which is related to the output image size (bigger=better but slower).
               If its value is too big matplotlib has problem with saving it.
        :param node_size: parameter, which is related to the intersections points sizes
        :param file_format: output file format
        """
        self.G = ox.graph_from_place(place, network_type=network_type)
        self.place = place
        self.removeDeadEnds()
        self.saveMapAsImage(file_name, fig_height, node_size, file_format)

    def removeDeadEnds(self):
        """
        Function removes dead-ends, because OSMNX threats them as intersestions.
        :return:
        """
        print(self.place)
        print('num of intersections before removing dead-ends:\t', len(self.G.nodes()))
        for node, count in self.G.graph['streets_per_node'].items():
            if count < 2:
                self.G.remove_node(node)
        print('num of intersections after removing dead-ends:\t', len(self.G.nodes()))

    def saveMapAsImage(self, file_name, fig_height, node_size, file_format):
        G_projected = ox.project_graph(self.G)
        print('projected')
        ox.plot_graph(G_projected, fig_height=fig_height, node_size=node_size, show=False, edge_linewidth=0, save=True,
                      filename=file_name, file_format=file_format)
        ox.plot_graph(G_projected, fig_height=fig_height, node_size=node_size, show=False, edge_linewidth=0.5, save=True,
                      filename=file_name+'_with_streets', file_format=file_format)


if __name__ == '__main__':
    OSMMapsDownloader('paris','Paris, France', network_type='multifractal_service', file_format='png', fig_height=70, node_size=0.01)
