import numpy as np
import math
from scipy import stats
import matplotlib.pyplot as plt


def log(x):
    return math.log(x)


class MultifractalParametersCalculator:
    def __init__(self, cities, plot_wrong=False, r_square_tolerance=0.9):
        self.cities = cities
        self.results = None
        self.plot_wrong = plot_wrong
        self.r_square_tolerance = r_square_tolerance

    def getAlphaParameter(self, q):
        """Linear regression eq (B3) in the article
         Equation 5. https://imagej.nih.gov/ij/plugins/fraclac/FLHelp/MultifractalOptions.htm#alpha
         """
        # sorting unnecessary, i used to remove 4 lowest and biggest points
        points = sorted(((log(city.box_size), city.getA(q)) for city in self.cities), key=lambda tup: tup[1])
        x, y = list(zip(*points))
        slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)

        if self.shouldPlot(r_value):
            title = "%s Q=%f R=%f" % ("alpha", q, r_value)
            self.plot(x, y, title, a=slope, b=intercept)
        return slope

    def shouldPlot(self, r_value):
        return self.plot_wrong and r_value ** 2 < self.r_square_tolerance

    def getTauParameter(self, q):
        """Regression - equations (9,10) in the article, Equation (B5) has no sense"""
        points = ((log(city.box_size), city.getTau(q)) for city in self.cities)
        x, y = list(zip(*points))
        slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)

        if self.shouldPlot(r_value):
            title = "%s Q=%f R=%f" % ("tau", q, r_value,)
            self.plot(x, y, title, a=slope, b=intercept)
        return slope

    def getTau_version2Parameter(self, q):
        """Equation 4. https://imagej.nih.gov/ij/plugins/fraclac/FLHelp/MultifractalOptions.htm#tau"""
        points = sorted(((log(city.box_size), log(city.getTau_version2(q))) for city in self.cities),
                        key=lambda tup: tup[1])
        x, y = list(zip(*points))
        slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)

        if self.shouldPlot(r_value):
            title = "%s Q=%f R=%f" % ("tau2", q, r_value,)
            self.plot(x, y, title, a=slope, b=intercept)
        return slope

    def getDParameter(self, q):
        """Equation (10) in the article"""
        if q == 1:
            return self.getDForQEqual1()
        else:
            return self.getTauParameter(q) / (q - 1)

    def getFAlphaParameter(self, q):
        """Regression of equetion (13) in the article"""
        points = sorted(((log(city.box_size), city.getF(q)) for city in self.cities), key=lambda tup: tup[1])
        x, y = list(zip(*points))
        slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)

        if self.shouldPlot(r_value):
            title = "%s Q=%f R=%f" % ("f(a)", q, r_value)
            self.plot(x, y, title, a=slope, b=intercept)
        return slope

    def getDForQEqual1(self):
        """Equetion (13) in the article"""
        points = sorted(((log(city.box_size), city.getDWhenQIs1()) for city in self.cities), key=lambda tup: tup[1])
        x, y = list(zip(*points))
        slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)

        if self.shouldPlot(r_value):
            title = "%s Q=%f R=%f" % ("D_q=1", 1., r_value)
            self.plot(x, y, title, a=slope, b=intercept)
        return slope

    def plot(self, x, y, plot_title, a=None, b=None):
        fig, ax = plt.subplots()
        if a is not None:
            x_for_fit_plot = np.linspace(min(x), max(x), 10)
            y_for_fit_plot = a * x_for_fit_plot + b
            ax.plot(x_for_fit_plot, y_for_fit_plot, '-', color='red')
        ax.scatter(x, y)
        plt.title(plot_title)
        plt.savefig(filename='fits/' + plot_title + '.png', format='png')
        plt.close()

    def calculate(self, q_generator):
        self.results = {}
        for q in q_generator:
            self.results[q] = {'alpha': self.getAlphaParameter(q),
                               'tau': self.getTauParameter(q),
                               'f(alpha)': self.getFAlphaParameter(q),
                               'D': self.getDParameter(q), }
            # print(q, self.getTauParameter(q), self.getTau_version2Parameter(q))

    def saveToDatafile(self, fname='default.dat'):
        with open(fname, 'w') as f:
            f.write('#q\talpha\ttau\tf(alpha)\tD\n')
            for q in sorted(self.results):
                f.write(str(q) + '\t%(alpha)f\t%(tau)f\t%(f(alpha))f\t%(D)f\n' % self.results[q])
