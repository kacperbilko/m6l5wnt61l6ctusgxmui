import re
import subprocess
import time
import math
import numpy as np
from CityIntersections import CityIntersections
from MultifractalParametersCalculator import MultifractalParametersCalculator
from OSMIntersectionsMapImageToNPArray import OSMIntersectionsMapImageToNPArray
from SlidingCounting2 import SlidingCounting2

if __name__ == '__main__':
    start_time = time.time()

    # If True regression plots will be created during calculations.
    plot_wrong = True
    # If plot_wrong is True, all fits which have r_value**2 < r_square_fit_quality will be plotted.
    r_square_fit_quality = 1

    image_path = 'images/moscow_with_streets_service_mode.png'
    coverage_ratio = 0.

    results_file_name = 'results/' + re.search(r'\/(.+)\.', image_path).group(0) + '_' + str(coverage_ratio) +  '.dat'
    osm_map_image = OSMIntersectionsMapImageToNPArray(image_path)
    intersections_array = osm_map_image.getArray()
    osm_map_image.saveImage(image_path[:-4]+'_bw.png')



    min_dimension = min(intersections_array.shape)

    # box_sizes - different methods
    # box_sizes = list(int(min_dimension/1.1**i) for i in range(1,40) if int(min_dimension/1.1**i) > 50)
    # box_sizes = np.linspace(0.2195 * min_dimension, 0.5*min_dimension, num=40, dtype=int)
    box_sizes = np.logspace(math.log(30,2), math.log(0.5 * min_dimension,2),num=40, base=2., dtype=int)


    print(box_sizes)
    cities = [CityIntersections(SlidingCounting2(intersections_array, box_size, int(box_size * coverage_ratio)))
              for box_size in box_sizes]

    for single_size_city in cities:
        single_size_city.run()

    # Parameters calculator
    calc = MultifractalParametersCalculator(cities, plot_wrong, r_square_fit_quality)
    q_values_generator = (q10/ 10 for q10 in range(-150, 151, 10))
    calc.calculate(q_values_generator)
    calc.saveToDatafile(results_file_name)

    # Make raw plot using gnuplot
    subprocess.Popen(['gnuplot', '-e', "datafile=\'" + results_file_name + "\'", 'script.dem'], shell=True)
    print(time.time() - start_time)
