from scipy import ndimage


class IntersectionsImageToNPArray(object):
    """
    Deprecated and currently not used class. It was needed when image analysis was performed by Fiji.
    """

    def __init__(self, image_file_path, mode='L'):
        self.city_map_as_bits = ndimage.imread(image_file_path, mode=mode)
        self.intersections_array = self.isIntersection(self.city_map_as_bits)

    def getArray(self):
        return self.intersections_array

    def isIntersection(self, x):
        return x == 255
