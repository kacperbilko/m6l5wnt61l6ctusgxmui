import numpy as np


class Box(object):
    """
    Box class represents box in box-counting method.
    """

    def __init__(self, intersections_number):
        """
        :param array: part of an image (seen as NumPy array), which corresponds with single box in box-counting method.
        :return:
        """
        self.intersections_number = intersections_number
        # self.setIntersectionsNumber(array)
        self.probability = None

    # def setIntersectionsNumber(self, array):
    #     """
    #     :param array: part of image (seen as NumPy array)...
    #     :return:
    #     """
    #     self.intersections_number = np.count_nonzero(array)

    def setProbability(self, total_intersections_number):
        """
        :param total_intersections_number: sum of intersections in all boxes
        :return:
        """
        self.probability = self.intersections_number / total_intersections_number
