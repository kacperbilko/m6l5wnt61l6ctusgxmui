import numpy as np


class BoxCounting(object):
    """
    Deprecated and not used class. SlidingCounting, with covering_size = 0, does almost the same.
    """

    def __init__(self, array, box_size):
        self.array_parts = None
        self.array = array
        self.box_size = box_size
        self.splitArray()

    def splitArray(self):
        self.array_parts = []
        oversize_horizontal = self.array.shape[1] % self.box_size
        oversize_vertical = self.array.shape[0] % self.box_size

        vertical_cuts = self.array.shape[0] // self.box_size
        horizontal_cuts = self.array.shape[1] // self.box_size

        top_idx = oversize_vertical // 2
        bottom_idx = self.array.shape[0] - (oversize_vertical + 1) // 2

        left_idx = oversize_horizontal // 2
        right_idx = self.array.shape[1] - (oversize_horizontal + 1) // 2

        split_array = (np.hsplit(vertical_split, horizontal_cuts) for vertical_split in
                       np.vsplit(self.array[top_idx:bottom_idx, left_idx:right_idx], vertical_cuts))

        for vertical_idx, vertical in enumerate(split_array):
            for horizontal_idx, array_part in enumerate(vertical):
                self.array_parts.append(array_part)

    def __len__(self):
        return len(self.array_parts)

    def __iter__(self):
        return BoxCountingIterator(self.array_parts)


class BoxCountingIterator(object):
    def __init__(self, array_parts):
        self.array_parts = array_parts
        self.i = 0

    def __next__(self):
        if self.i < len(self.array_parts):
            i = self.i
            self.i += 1
            return self.array_parts[i]
        else:
            raise StopIteration()


if __name__ == '__main__':
    pass
