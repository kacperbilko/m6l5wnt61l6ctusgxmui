import cv2, numpy as np


class OSMIntersectionsMapImageToNPArray(object):
    def __init__(self, file_path):
        """
        :param file_path: path to image, downloaded by OSMMapsDownloader. Image background must be white.
        """
        '''0 means read in gray-scale'''
        self.img = cv2.imread(file_path, 0)

        '''everything, which is not white is considered as an intersection'''
        _, self.img = cv2.threshold(self.img, 254, 255, cv2.THRESH_BINARY_INV)

        '''As the result self.img has black background(value in array 0) and white shapes(value 255)'''

    def setWhiteToIntersectionsPointsCenters(self, centers):
        """
        Function overrides array with an image with "black" background and sets white pixels (=cells of an array) in
        shapes (intersections points) centers.
        :param centers: Indexes of white shapes centers (= intersections points)
        :return:
        """
        row, column = list(zip(*centers))
        self.img = np.zeros(self.img.shape, dtype=np.uint8)

        '''set "white" to intersections centers'''
        self.img[np.array(column), np.array(row)] = 255

    def setIntersectionsPointsCenters(self):
        """
        Function finds contours and their centers using moments (similar like in finding center of mass point)
        :return:
        """
        _, contours, _ = cv2.findContours(self.img.copy(), cv2.RETR_CCOMP, cv2.CHAIN_APPROX_TC89_L1)
        centres = []
        for contour in contours:
            moments = cv2.moments(contour)
            centres.append(
                (int(moments['m10'] / moments['m00']), int(moments['m01'] / moments['m00']))
                if moments['m00'] else
                contour[0][0])
        self.setWhiteToIntersectionsPointsCenters(centres)

    def getArray(self):
        self.setIntersectionsPointsCenters()
        return np.array(self.img, dtype=np.bool_)

    def saveImage(self, file_path):
        cv2.imwrite(file_path, self.img)


if __name__ == '__main__':
    foo = OSMIntersectionsMapImageToNPArray('images/london_service.png')
    print(np.count_nonzero(foo.getArray()), foo.getArray().shape)
    foo.saveImage('images/london_service_bw.png')
