import numpy as np


class SlidingCounting2(object):
    def __init__(self, array, box_size, covering_size=0, omit_empty=True):
        """
        SlidingCounting2 object has an iterator, which allows to iterate over array parts, made from the image,
        other functions are for internal use.
        :param array: Input array to split (whole image as NP array).
        :param box_size: "epsilon" in box-counting method, given in pixels.
        :param covering_size: number of common pixels (for adjacent boxes)
        :param omit_empty: if True, iterator returns only non-empty array parts.
        """
        self.indexes = None
        self.omit_empty = omit_empty
        self.array = array
        self.box_size = box_size
        self.covering_size = covering_size
        self.generateBoxesIndexes()

    def generateBoxesIndexes(self):
        """
        Function creates  boundary indexes of array parts.
        :return:
        """
        self.indexes = []
        if self.box_size == self.covering_size:
            raise Exception("Box size must be different form covering size!")

        for row_idx in range(0, self.array.shape[0], self.box_size - self.covering_size):
            for column_idx in range(0, self.array.shape[1], self.box_size - self.covering_size):
                self.indexes.append({'start_row': row_idx, 'end_row': row_idx + self.box_size,
                                    'start_column': column_idx, 'end_column': column_idx + self.box_size})


    def __iter__(self):
        return SlidingCounting2Iterator(self.array, self.indexes, self.omit_empty)


class SlidingCounting2Iterator(object):
    def __init__(self, array, indexes, omit_empty):
        self.indexes = indexes
        self.array = array
        self.i = 0
        self.omit_empty = omit_empty

    def __next__(self):
        while self.i < len(self.indexes):
            idx = self.indexes[self.i]
            result = self.array[idx['start_row']: idx['end_row'], idx['start_column']: idx['end_column']]
            self.i += 1
            num_of_intersections = np.count_nonzero(result)
            if self.omit_empty and num_of_intersections:
                return num_of_intersections
            elif not self.omit_empty:
                return num_of_intersections
        raise StopIteration()


if __name__ == '__main__':
    foo = np.arange(4 * 4).reshape(4, 4)
    # foo[4, 20] = 1
    # foo[5, 26] = 0
    sc = SlidingCounting2(foo, 3, 2)
    for i in sc:
        print(i)
